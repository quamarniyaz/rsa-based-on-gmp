#include "myheader.h"

int main (int argc, char * argv[]) {

	if(argc != 3 ){
		printf("<%s msgfile decryptfile\n",argv[0]) ;
		exit(-1) ;
	}
	
	int myRank, commSize;
	
	//Initialize MPI
	MPI_Init(&argc, &argv);
	MPI_Comm_rank (MPI_COMM_WORLD, &myRank);
    MPI_Comm_rank (MPI_COMM_WORLD, &commSize);

	// Initialize n, public, private keys in big numbers 
	// after reading them from their respective files. 
	mpz_t pq, public, private ;
	mpz_init(pq) ;
	mpz_init(public) ;
	mpz_init(private) ;
	findKey(myRank,"public2.key", pq, public) ;
	findKey(myRank,"private2.key", pq, private) ;

	// Creates MPI file for message and decrypted 
	// message files
	MPI_File msgFile ;
	MPI_File decryptFile ;
	MPI_Info msgInfo ;
	MPI_Info decryptInfo ;
	MPI_Datatype contig ;
	createMsgFile(argv[1], myRank, &msgFile, &msgInfo, &contig, MPI_MODE_RDONLY) ;
	createMsgFile(argv[2], myRank, &decryptFile, &decryptInfo, &contig, MPI_MODE_CREATE|MPI_MODE_WRONLY) ;


	int count, outBlockSize ;
	char buffer[SIZE] ;
	char *outBuffer ;
	do{
		// Read blocks from msg file
		count = readWriteBlocks(buffer, &msgFile, contig, 0) ;
		if( count == 0 )
			break ;
		// Convert blocks into big numbers
		mpz_t msg;
		mpz_init(msg);
		mpz_import(msg, count, 1, 1, 0, 0, buffer);
							
		// Encode blocks in big numbers
		mpz_t encodedData ;
		mpz_init(encodedData) ;		
		crypto(pq, public, msg, encodedData) ;
		
		// Decode blocks in big numbers	
		mpz_t decodedData ;
		mpz_init(decodedData) ;	
		crypto(pq, private, encodedData, decodedData); 
		
		// Convert decoded data into msg file format	
		// and writes into decrypted message file

		outBuffer = (char *)mpz_export(NULL, (size_t *)&outBlockSize, 1, 1, 1, 0, decodedData);
		readWriteBlocks(outBuffer, &decryptFile, contig,1);
		
		// prints the decrypted file in console
		write(1, outBuffer, outBlockSize) ;
		free(outBuffer) ;
	}while(1) ;	

	MPI_File_close(&msgFile);
	MPI_File_close(&decryptFile);
	return 0;
}
