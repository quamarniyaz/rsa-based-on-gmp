#include <mpi.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

	/*
	 	*  Sample MPI based program to read from a file.  The reads are done as
	 	*  as one block each through a file.  It doesn't do anything else very
	 	*  interesting at the moment.
	*/

int main(int argCount, char *argValues [] ) {
	MPI_File fileHandle;
	MPI_Info info;
	char *localArray;
	int  rc, myRank, commSize;

	MPI_Init(0,NULL);
	MPI_Comm_rank (MPI_COMM_WORLD, &myRank);
    MPI_Comm_rank (MPI_COMM_WORLD, &commSize);

	/* Create an info object for the file and then open
 	 * the file.  I'm using a fixed name file here to make the program
 	 * as simple as possible
 	*/
	
	MPI_Info_create(&info);
	MPI_Info_set(info, "romio_cb_read", "enable");
	rc = MPI_File_open(MPI_COMM_WORLD, "sample.txt", MPI_MODE_RDONLY, info, &fileHandle);
	if (rc != MPI_SUCCESS) {
		fprintf(stderr, "MPI file open failed");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	/*
 	 * The file is open.  Now create the datatype for the file, set up the 
 	 * offset for the file.
 	*/
	
	MPI_Datatype contig;
	int LOCAL_BUFFER_SIZE = 3;

	MPI_Type_contiguous(LOCAL_BUFFER_SIZE, MPI_CHAR, &contig);
	MPI_Type_commit(&contig);
	MPI_Offset disp;
	disp = myRank *LOCAL_BUFFER_SIZE * 2 * sizeof(char);
	rc = MPI_File_set_view(fileHandle, disp, contig, contig, "native", info);
	
	if (rc != MPI_SUCCESS) {
		fprintf(stderr,"MPI file set view failed") ;
		MPI_Abort(MPI_COMM_WORLD, 2);
	}
	/*
	 ** Set up a buffere and read a block forom the file.  
	*/

	char buffer[3];
	MPI_Status status;
	int count;
	int i ;
	do{
		rc = MPI_File_read(fileHandle, buffer, 1, contig, &status);
		if (rc != MPI_SUCCESS){
			fprintf(stderr,"MPI_file_read failed"); 
			MPI_Abort(MPI_COMM_WORLD, 3);
		}
		else {
			fflush(stdout);
			MPI_Get_count(&status, MPI_CHAR, &count);
			write(1, buffer, count);
			printf("\n") ;
		}
	}while(count > 0) ;

/*
 *  * close up MPI
 */

	MPI_File_close(&fileHandle);
	MPI_Finalize();
	return EXIT_SUCCESS;
}

