#include "myheader.h"

void findKey(int myRank, char *fileName, mpz_t pq_t, mpz_t key_t){
	int rc ;
	char pq[KSIZE], key[KSIZE] ; 
	
	int  i = 0, nlc = 0 ;

	for(i = 0 ; i < KSIZE ; i++)
		pq[i] = '\0' ;
	for(i = 0 ; i < KSIZE ; i++)
		key[i] = '\0' ;


	MPI_File fileHandle;
	MPI_Info info ;

	MPI_Info_create(&info);
	MPI_Info_set(info, "romio_cb_read", "enable");
	rc = MPI_File_open(MPI_COMM_WORLD, fileName, MPI_MODE_RDONLY, info, &fileHandle);
	if (rc != MPI_SUCCESS) {
		fprintf(stderr, "MPI file open failed");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	MPI_Datatype contig;
	int LOCAL_BUFFER_SIZE = 1;

	MPI_Type_contiguous(LOCAL_BUFFER_SIZE, MPI_CHAR, &contig);
	MPI_Type_commit(&contig);
	MPI_Offset disp;
	disp = myRank *LOCAL_BUFFER_SIZE * 2 * sizeof(char);
	rc = MPI_File_set_view(fileHandle, disp, contig, contig, "native", info);
	
	if (rc != MPI_SUCCESS) {
		fprintf(stderr,"MPI file set view failed") ;
		MPI_Abort(MPI_COMM_WORLD, 2);
	}
	
	char buffer[1];
	MPI_Status status;
	int count;
	
	do{
		rc = MPI_File_read(fileHandle, buffer, 1, contig, &status);
		if (rc != MPI_SUCCESS){
			fprintf(stderr,"MPI_file_read failed"); 
			MPI_Abort(MPI_COMM_WORLD, 3);
		}
		else {
			if(buffer[0] == '\n'){
				if(nlc == 1)
					break ;
				nlc++ ;
				continue ;
			}
			char c[2] ;
			c[0] = buffer[0] ;
			c[1] = '\0' ;
			if(nlc == 0)
				strcat(pq, c) ;
			else
				strcat(key, c);
			MPI_Get_count(&status, MPI_CHAR, &count);
		}
	}while(count > 0) ;
	
	mpz_t pqn ;
	mpz_init(pqn) ;
	mpz_t keyn ;
	mpz_init(keyn) ;
	
	mpz_set_str(pqn, pq, 10) ;
	mpz_set_str(keyn, key, 10) ;
	mpz_set(pq_t, pqn) ;
	mpz_set(key_t, keyn) ;

	MPI_File_close(&fileHandle);
	return ;
}

void crypto(mpz_t pq, mpz_t key, mpz_t msg, mpz_t encDecMsg){
	mpz_t tmp  ;
	mpz_init(tmp) ;	
	mpz_powm(tmp, msg, key, pq) ;
	mpz_set(encDecMsg, tmp) ;
	return ;
}
