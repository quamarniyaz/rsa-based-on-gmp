#include "myheader.h"

void createMsgFile(char *fileName, int myRank, MPI_File *fileHandle, MPI_Info *info, MPI_Datatype *contig, int mode ){
	int rc ;	
	MPI_Info_create(info);
	MPI_Info_set(*info, "romio_cb_read", "enable");
	rc = MPI_File_open(MPI_COMM_WORLD, fileName, mode, *info, fileHandle);
	if (rc != MPI_SUCCESS) {
		fprintf(stderr, "MPI file open failed");
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	MPI_Type_contiguous(SIZE, MPI_CHAR, contig);
	MPI_Type_commit(contig);
	MPI_Offset disp;
	disp = myRank * SIZE * 2 * sizeof(char);
	rc = MPI_File_set_view(*fileHandle, disp, *contig, *contig, "native", *info);
	
	if (rc != MPI_SUCCESS) {
		fprintf(stderr,"MPI file set view failed") ;
		MPI_Abort(MPI_COMM_WORLD, 2);
	}
	
}

int readWriteBlocks(char *buffer, MPI_File *fileHandle, MPI_Datatype contig, int type){

	MPI_Status status;
	int count, rc;
	if(type == 0)	
		rc = MPI_File_read(*fileHandle, buffer, 1, contig, &status);
	else
		rc = MPI_File_write(*fileHandle, buffer, 1, contig, &status);
	if (rc != MPI_SUCCESS){
		fprintf(stderr,"MPI_file_read failed"); 
		MPI_Abort(MPI_COMM_WORLD, 3);
	}
	else {
		fflush(stdout);
		MPI_Get_count(&status, MPI_CHAR, &count) ;
	}
	return count ;	
}
