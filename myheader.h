#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <gmp.h>
#include <mpi.h>
#include <string.h>

#define SIZE   128
#define KSIZE  1133 	

void findKey(int , char *, mpz_t, mpz_t)  ;
void crypto( mpz_t, mpz_t, mpz_t, mpz_t ) ;
void createMsgFile(char *file, int, MPI_File *, MPI_Info *, MPI_Datatype *, int ) ;
int readWriteBlocks(char *, MPI_File *, MPI_Datatype, int) ;
